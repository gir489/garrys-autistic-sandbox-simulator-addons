local last_wep = NULL
local cur_wep = NULL
local lastinv_last_weapon = CreateClientConVar("lastinv_last_weapon", "") --This is to help with changing levels clearing the Lua script locals. I wish there was a way to store this without the client seeing it in the console.
local lastinv_new_frequency = CreateClientConVar("lastinv_new_frequency", "0.05", true, false, "The delay interval in seconds to check for a new weapon. If the delay is too small, the timer will fire on the next frame/tick.")
 
concommand.Add( "lastinv_new", function()
    if last_wep == NULL then
		local weapon = LocalPlayer():GetWeapon(lastinv_last_weapon:GetString())
		if IsValid(weapon) then
			input.SelectWeapon(weapon)
		end
		return
	end
    if not last_wep:IsValid() then return end
   
    input.SelectWeapon(last_wep)
end)

local function timer_func()
	local local_player = LocalPlayer()
	if IsValid(local_player) then
		local cur_wep_new = local_player:GetActiveWeapon()
		if cur_wep != cur_wep_new then
			last_wep = cur_wep
			if IsValid(last_wep) then
				lastinv_last_weapon:SetString(last_wep:GetClass())
			end
			cur_wep = cur_wep_new
		end
	end
end

cvars.RemoveChangeCallback( "lastinv_new_frequency", "lastinv_new_frequency_callback" )
cvars.AddChangeCallback( "lastinv_new_frequency", function( convar_name, value_old, value_new )
	timer.Adjust( "lastinv_timer", lastinv_new_frequency:GetFloat(), 0, timer_func )
end, "lastinv_new_frequency_callback" )

timer.Create( "lastinv_timer", lastinv_new_frequency:GetFloat(), 0, timer_func )