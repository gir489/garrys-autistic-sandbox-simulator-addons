AddCSLuaFile()
DEFINE_BASECLASS( "base_anim" )

ENT.Type = "anim"
ENT.Spawnable = false
ENT.ExplodeTimer = CurTime()

function ENT:Draw()
	self.Entity:DrawModel()
end

function ENT:Initialize()
	if SERVER then
		self:SetModel( "models/ns/weapons/w_grenade.mdl" )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:DrawShadow( false )
	end
	self.ExplodeTimer = CurTime() + 2.3
end

function ENT:Think()
	if SERVER then
		if self.ExplodeTimer <= CurTime() then
			self:Remove()
		end
	end
	self:NextThink( CurTime() )
	return true
end

function ENT:PhysicsCollide( data )
	if SERVER then
		if data.HitEntity:IsNPC() || data.HitEntity:IsPlayer() then
			self.ExplodeTimer = CurTime()
			return
		end
		if data.Speed > 50 then
			self:EmitSound( "Weapon_NS_Grenade.Bounce" )
		end
	end
end

function ENT:OnRemove()
	if SERVER then
		local effectdata = EffectData()
		effectdata:SetOrigin(self:GetPos())
		util.Effect("HelicopterMegaBomb", effectdata, true, true)
	end
	self:EmitSound("NS/weapons/grenade/explode" .. math.random(3,5) .. ".wav", 100) --Sum Ting Wong with soundscapes causing the sound to be very quiet.
	util.BlastDamage( self, self.Owner, self:GetPos(), 275, 125 ) --Bang Ding Ow.
end