--Pistol
sound.Add(
{
name = "Weapon_NS_Pistol.Single",
channel = CHAN_WEAPON,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = "NS/weapons/pistol/hg-1.wav"
})
sound.Add(
{
name = "Weapon_NS_Pistol.SlideRelease",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/pistol/pistol_slide_release.wav"
})
sound.Add(
{
name = "Weapon_NS_Pistol.ClipIn",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/pistol/pistol_clipin.wav"
})
sound.Add(
{
name = "Weapon_NS_Pistol.ClipOut",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/pistol/pistol_clipout.wav"
})
sound.Add(
{
name = "Weapon_NS_Pistol.Draw",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/pistol/pistol_draw.wav"
})

--Lmg
sound.Add(
{
name = "Weapon_NS_Lmg.Single",
channel = CHAN_WEAPON,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = "NS/weapons/lmg/mg-1.wav"
})
sound.Add(
{
name = "Weapon_NS_Lmg.ClipIn",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/lmg/lmg_clipin.wav"
})
sound.Add(
{
name = "Weapon_NS_Lmg.ClipOut",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/lmg/lmg_clipout.wav"
})
sound.Add(
{
name = "Weapon_NS_Lmg.Draw",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/lmg/lmg_draw.wav"
})

--Grenade Gun
sound.Add(
{
name = "Weapon_NS_GG.Single",
channel = CHAN_WEAPON,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = "NS/weapons/gg/gg-1.wav"
})
sound.Add(
{
name = "Weapon_NS_GG.ReloadStart",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/gg/gg_reload_start.wav"
})
sound.Add(
{
name = "Weapon_NS_GG.Insert",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/gg/gg_insert.wav"
})
sound.Add(
{
name = "Weapon_NS_GG.ReloadEnd",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/gg/gg_reload_end.wav"
})
sound.Add(
{
name = "Weapon_NS_GG.Draw",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/gg/gg_draw.wav"
})
sound.Add(
{
name = "Weapon_NS_GG.Rotate",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/gg/gg_rotate.wav"
})

--Grenades
--[[sound.Add(
{
name = "Weapon_NS_Grenade.Explode",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = {"NS/weapons/grenade/explode3.wav",
		 "NS/weapons/grenade/explode4.wav",
		 "NS/weapons/grenade/explode5.wav"}
})]]
sound.Add(
{
name = "Weapon_NS_Grenade.Bounce",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = {"NS/weapons/grenade/grenade_hit1.wav",
		 "NS/weapons/grenade/grenade_hit2.wav",
		 "NS/weapons/grenade/grenade_hit3.wav"}
})

--Shotgun
sound.Add(
{
name = "Weapon_NS_Shotgun.Single",
channel = CHAN_WEAPON,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = "NS/weapons/shotgun/sg-1.wav"
})
sound.Add(
{
name = "Weapon_NS_Shotgun.Reload",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/shotgun/shotgun_reload.wav"
})
sound.Add(
{
name = "Weapon_NS_Shotgun.Pump",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/shotgun/shotgun_pump.wav"
})
sound.Add(
{
name = "Weapon_NS_Shotgun.Draw",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/shotgun/shotgun_draw.wav"
})

--HMG
sound.Add(
{
name = "Weapon_NS_HMG.Single",
channel = CHAN_WEAPON,
volume = VOL_NORM,
soundlevel = SNDLVL_GUNFIRE,
sound = "NS/weapons/hmg/hmg-1.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.Open",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_open.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.ClipOut",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_clipout.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.ClipIn",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_clipin.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.Close",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_close.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.Slide",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_slide.wav"
})
sound.Add(
{
name = "Weapon_NS_HMG.Draw",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/hmg/hmg_draw.wav"
})

--Shared
sound.Add(
{
name = "Weapon_NS/weapons.DryFire",
channel = CHAN_AUTO,
volume = VOL_NORM,
soundlevel = SNDLVL_NORM,
sound = "NS/weapons/ns_dry_fire.wav"
})