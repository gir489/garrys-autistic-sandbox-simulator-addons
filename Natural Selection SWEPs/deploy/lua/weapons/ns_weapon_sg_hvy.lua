--Reimplementation of AvHBasePlayerWeapon.cpp in Garry's Mod SWEP Lua.

if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "ns/hud/Shotgun" )
SWEP.DrawWeaponInfoBox = false
SWEP.BounceWeaponIcon = false
killicon.Add( "ns_weapon_sg_hvy", "ns/hud/Shotgun_Killicon", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Shotgun (Heavy)"
SWEP.Category = "Natural-Selection"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 80
SWEP.ViewModel = "models/ns/weapons/v_sg_hvy.mdl"
SWEP.WorldModel = "models/ns/weapons/w_sg.mdl"
SWEP.ViewModelFlip = false
SWEP.BobScale = 0
SWEP.SwayScale = 0

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 7
SWEP.Slot = 3
SWEP.SlotPos = 0

SWEP.UseHands = false
SWEP.HoldType = "crossbow"
SWEP.FiresUnderwater = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 0
SWEP.Base = "weapon_base"
SWEP.NeedsDryfireNoise = true

SWEP.Reloading = 0
SWEP.ReloadingTimer = CurTime()
SWEP.Shelling = 0
SWEP.ShellTimer = CurTime()

SWEP.Primary.Sound = Sound( "Weapon_NS_Shotgun.Single" )
SWEP.Primary.ClipSize = 8
SWEP.Primary.DefaultClip = 8
SWEP.Primary.MaxAmmo = 40
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "Buckshot"
SWEP.Primary.Damage = 17
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.NumberofShots = 10
SWEP.Primary.Spread = 1
SWEP.Primary.Delay = 0.65
SWEP.Primary.Force = 5
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
	self:SetWeaponHoldType( self.HoldType )
	self:SetNextIdle()
end

function DrawCrosshairForeground(x, y)
	surface.SetDrawColor( 255, 255, 255, 255 )
	surface.DrawRect( x-1, y-9, 2, 6 ) --Up
	surface.DrawRect( x-1, y+3, 2, 6 ) --Down
	surface.DrawRect( x-9, y-1, 6, 2 ) --Left
	surface.DrawRect( x+3, y-1, 6, 2 ) --Right
end

function DrawCrosshairBackground(x, y)
	surface.SetDrawColor( 0, 0, 0, 255 )
	surface.DrawRect( x-2, y-10, 4, 7 ) --Up
	surface.DrawRect( x-2, y+3, 4, 7 ) --Down
	surface.DrawRect( x-10, y-2, 7, 4 ) --Left
	surface.DrawRect( x+3, y-2, 7, 4 ) --Right
end

function SWEP:DoDrawCrosshair( x, y ) --Draw the NS crosshair without using sprites.
	DrawCrosshairBackground(x, y)
	DrawCrosshairForeground(x, y)
	return true
end

function SWEP:Deploy()
	self:SetWeaponHoldType( self.HoldType )
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetNextPrimaryFire( CurTime() + 0.9 )
	self.Reloading = 0
	self.ReloadingTimer = CurTime()
	self.Shelling = 0
	self:SetNextIdle()
	return true
end

function SWEP:Holster()
	self.Reloading = 0
	self.ReloadingTimer = CurTime()
	self.Shelling = 0
	return true
end

function SWEP:PrimaryAttack()
	if self.Weapon:Clip1() <= 0 and self.Weapon:Ammo1() <= 0 then
		if self.NeedsDryfireNoise == true then
			self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_EMPTY )
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	end
	if self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then
		if self.NeedsDryfireNoise == true then
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	end
	if self.Weapon:Clip1() <= 0 and self.Reloading == 0 then
		self:EmitSound( "Weapon_NS/weapons.DryFire" )
		self:Reload()
		return
	end
	local bullet = {}
	bullet.Num = self.Primary.NumberofShots
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
	bullet.Spread = Vector( 0.1 * self.Primary.Spread, 0.1 * self.Primary.Spread, 0 )
	bullet.Tracer = 0
	bullet.Force = self.Primary.Force
	bullet.Damage = self.Primary.Damage
	bullet.AmmoType = self.Primary.Ammo
	self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self.Owner:FireBullets( bullet )
	self.Owner:EmitSound( self.Primary.Sound )
	self:TakePrimaryAmmo( self.Primary.TakeAmmo )
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self.Reloading = 0
	self.ReloadingTimer = CurTime()
	self.Shelling = 0
	self:SetNextIdle()
end

function SWEP:Reload()
	if self.Reloading == 0 and self.Weapon:Clip1() < self.Primary.ClipSize and self.Weapon:Ammo1() > 0 then
		self.Weapon:SendWeaponAnim( ACT_VM_RELOAD_IDLE )
		self.Owner:SetAnimation( PLAYER_RELOAD )
		self.Reloading = 1
		self.ReloadingTimer = CurTime() + 1.1
		if self.Weapon:Clip1() <= 0 then
			self:SetNextPrimaryFire( CurTime() + 1.66 )
		else
			self:SetNextPrimaryFire( CurTime() + 1.0 )
		end
		self:SetNextIdle()
	end
end

function SWEP:SecondaryAttack()
end

function SWEP:Think()
	local NotAttacking = self.Owner:KeyDown(IN_ATTACK) == false
	if SERVER then
		if self.Shelling == 1 and self.ShellTimer <= CurTime() then
			self.Shelling = 0
			self.ShellTimer = CurTime()
			self.Weapon:SetClip1( self.Weapon:Clip1() + 1 )
			self.Owner:RemoveAmmo( 1, self.Primary.Ammo, false )
		end
		if self.Reloading == 1 and self.ReloadingTimer <= CurTime() and self.Weapon:Clip1() < self.Primary.ClipSize and self.Weapon:Ammo1() > 0 then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
			self.Reloading = 1
			self.ReloadingTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
			self:RequestNewShell()
			self:SetNextIdle()
		end
		if self.Reloading == 1 and self.ReloadingTimer <= CurTime() and self.Weapon:Clip1() == self.Primary.ClipSize then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOADEMPTY )
			self.Reloading = 0
			self.ReloadingTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
			self:SetNextIdle()
		end
		if self.Reloading == 1 and self.ReloadingTimer <= CurTime() and self.Weapon:Clip1() > 0 and self.Weapon:Ammo1() <= 0 then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOADEMPTY )
			self.Reloading = 0
			self.ReloadingTimer = CurTime() + self.Owner:GetViewModel():SequenceDuration()
			self:SetNextIdle()
		end
		if self.Weapon:Ammo1() > self.Primary.MaxAmmo then
			self.Owner:SetAmmo( self.Primary.MaxAmmo, self.Primary.Ammo )
		end
	end
	if self.Weapon:Clip1() <= 0 and self.Reloading == 0 and (self.Weapon:GetNextPrimaryFire() < CurTime()) and NotAttacking then
		self:Reload()
	end
	if self.IdleTimer < CurTime() then
		if SERVER then
			self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
		end
		self:SetNextIdle()
	end
	if not self.NeedsDryfireNoise and NotAttacking then
		self.NeedsDryfireNoise = true
	end
end

function SWEP:SetNextIdle()
	self.IdleTimer = CurTime() + util.SharedRandom("NS_IDLE_TIMER", 5, 7.5)
end

function SWEP:RequestNewShell()
	self.Shelling = 1
	self.ShellTimer = CurTime() + 0.22
end