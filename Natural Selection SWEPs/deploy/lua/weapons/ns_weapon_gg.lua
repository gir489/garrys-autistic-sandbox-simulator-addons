--Reimplementation of AvHBasePlayerWeapon.cpp in Garry's Mod SWEP Lua.

if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "ns/hud/gg" )
SWEP.DrawWeaponInfoBox = false
SWEP.BounceWeaponIcon = false
killicon.Add( "ns_weapon_gg", "ns/hud/gg_Killicon", Color( 255, 255, 255, 255 ) )
killicon.Add( "ns_grenade_gun_grenade", "ns/hud/gg_Killicon", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Grenade Launcher"
SWEP.Category = "Natural-Selection"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 80
SWEP.ViewModel = "models/ns/weapons/v_gg.mdl"
SWEP.WorldModel = "models/ns/weapons/w_gg.mdl"
SWEP.ViewModelFlip = false
SWEP.BobScale = 0
SWEP.SwayScale = 0

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 7
SWEP.Slot = 3
SWEP.SlotPos = 0

SWEP.UseHands = false
SWEP.HoldType = "crossbow"
SWEP.FiresUnderwater = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 0
SWEP.Base = "weapon_base"
SWEP.NeedsDryfireNoise = true

SWEP.Primary.Sound = Sound( "Weapon_NS_GG.Single" )
SWEP.Primary.ClipSize = 4
SWEP.Primary.DefaultClip = 4
SWEP.Primary.MaxAmmo = 30
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SMG1_Grenade"
SWEP.Primary.Damage = 125
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Spread = 0
SWEP.Primary.Delay = 0.6
SWEP.Primary.Force = 800
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
	self:SetWeaponHoldType( self.HoldType )
	self:SetNextIdle(0.0)
end

function DrawCrosshairForeground(x, y)
	surface.SetDrawColor( 255, 255, 255, 255 )
	surface.DrawRect( x-1, y-9, 2, 6 ) --Up
	surface.DrawRect( x-1, y+3, 2, 6 ) --Down
	surface.DrawRect( x-9, y-1, 6, 2 ) --Left
	surface.DrawRect( x+3, y-1, 6, 2 ) --Right
end

function DrawCrosshairBackground(x, y)
	surface.SetDrawColor( 0, 0, 0, 255 )
	surface.DrawRect( x-2, y-10, 4, 7 ) --Up
	surface.DrawRect( x-2, y+3, 4, 7 ) --Down
	surface.DrawRect( x-10, y-2, 7, 4 ) --Left
	surface.DrawRect( x+3, y-2, 7, 4 ) --Right
end

function SWEP:DoDrawCrosshair( x, y ) --Draw the NS crosshair without using sprites.
	DrawCrosshairBackground(x, y)
	DrawCrosshairForeground(x, y)
	return true
end

function SWEP:Deploy()
	self:SetWeaponHoldType( self.HoldType )
	if self.Weapon:Clip1() == 3 then
		self.Weapon:SendWeaponAnim( ACT_VM_DRAW_DEPLOYED )
	elseif self.Weapon:Clip1() == 2 then
		self.Weapon:SendWeaponAnim( ACT_VM_DRAW_M203 )
	elseif self.Weapon:Clip1() == 1 then
		self.Weapon:SendWeaponAnim( ACT_VM_DRAW_EMPTY )
	else
		self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	end
	self:SetNextPrimaryFire( CurTime() + .5 )
	self.Reloading = 0
	self.ReloadingTimer = CurTime()
	self:SetNextIdle(0.0)
	if self.Owner.NSSkinColor == nil then
		self.Owner.NSSkinColor = math.random(0,1)
	end
	self.Owner:GetViewModel():SetSkin(self.Owner.NSSkinColor)
	return true
end

function SWEP:Holster()
	return true
end

function SWEP:PrimaryAttack()
	if self.Reloading == 1 then return end	
	if self.Weapon:Clip1() <= 0 and self.Weapon:Ammo1() <= 0 then
		if self.NeedsDryfireNoise == true then
			self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_EMPTY )
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	elseif self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then
		if self.NeedsDryfireNoise == true then
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	elseif self.Weapon:Clip1() <= 0 then
		self:EmitSound( "Weapon_NS/weapons.DryFire" )
		self:Reload()
		return
	end
	if SERVER then
		local entity = ents.Create( "ns_grenade_gun_grenade" )
		entity:SetOwner( self.Owner )
		if IsValid( entity ) then
			local Forward = self.Owner:EyeAngles():Forward()
			local Right = self.Owner:EyeAngles():Right()
			local Up = self.Owner:EyeAngles():Up()
			entity:SetPos( self.Owner:GetShootPos() + Forward * 8 + Right * 4 + Up * -4 )
			entity:SetAngles( self.Owner:EyeAngles() )
			entity:Spawn()
			local phys = entity:GetPhysicsObject()
			phys:SetVelocity( self.Owner:GetAimVector() * self.Primary.Force )
			phys:AddAngleVelocity( Vector( math.Rand( -500, 500 ), math.Rand( -500, 500 ), math.Rand( -500, 500 ) ) )
		end
	end
	self.Owner:EmitSound( self.Primary.Sound )
	if self.Weapon:Clip1() == 4 then
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_4 )
	elseif self.Weapon:Clip1() == 3 then
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_3 )
	elseif self.Weapon:Clip1() == 2 then
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_2 )
	elseif self.Weapon:Clip1() == 1 then
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_1 )
	else
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_EMPTY )
	end
	self:TakePrimaryAmmo( self.Primary.TakeAmmo )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:SetNextIdle(0.0)
end

function SWEP:Reload()
	if self.Reloading == 0 and self.Weapon:Clip1() < self.Primary.ClipSize and self.Weapon:Ammo1() > 0 then
		if self.Weapon:Clip1() == 3 then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
		elseif self.Weapon:Clip1() == 2 then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOAD_EMPTY )
		elseif self.Weapon:Clip1() == 1 then
			self.Weapon:SendWeaponAnim( ACT_VM_RELOAD_IDLE )
		else
			self.Weapon:SendWeaponAnim( ACT_VM_RELOADEMPTY )
		end
		self.Owner:SetAnimation( PLAYER_RELOAD )
		self.Reloading = 1
		self.ReloadingTimer = CurTime() + 1.10+1.10*(4-self.Weapon:Clip1())+1.10
		self:SetNextPrimaryFire(self.ReloadingTimer)
		self:SetNextIdle(self.Owner:GetViewModel():SequenceDuration() - 2)
	end
end

function SWEP:SecondaryAttack()
end

function SWEP:Think()
	if self.Weapon:Clip1() <= 0 and self.Reloading == 0 and (self.Weapon:GetNextPrimaryFire() < CurTime()) then
		self:Reload()
	end
	if self.Reloading == 1 and self.ReloadingTimer <= CurTime() then
		if self.Weapon:Ammo1() > ( self.Primary.ClipSize - self.Weapon:Clip1() ) then
			self.Owner:SetAmmo( self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1(), self.Primary.Ammo )
			self.Weapon:SetClip1( self.Primary.ClipSize )
		end
		if (self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1()) + self.Weapon:Clip1() < self.Primary.ClipSize then
			self.Weapon:SetClip1( self.Weapon:Clip1() + self.Weapon:Ammo1() )
			self.Owner:SetAmmo( 0, self.Primary.Ammo )
		end
		self.Reloading = 0
	end
	if self.IdleTimer < CurTime() then
		if SERVER then
			if self.Weapon:Clip1() == 3 then
				self.Weapon:SendWeaponAnim( ACT_VM_IDLE_3 )
			elseif self.Weapon:Clip1() == 2 then
				self.Weapon:SendWeaponAnim( ACT_VM_IDLE_2 )
			elseif self.Weapon:Clip1() == 1 then
				self.Weapon:SendWeaponAnim( ACT_VM_IDLE_1 )
			else
				self.Weapon:SendWeaponAnim( ACT_VM_IDLE_4 )
			end
		end
		self:SetNextIdle(0.0)
	end
	if self.Weapon:Ammo1() > self.Primary.MaxAmmo then
		self.Owner:SetAmmo( self.Primary.MaxAmmo, self.Primary.Ammo )
	end
	if not self.NeedsDryfireNoise and self.Owner:KeyDown(IN_ATTACK) == false then
		self.NeedsDryfireNoise = true
	end
end

function SWEP:SetNextIdle(offset)
	self.IdleTimer = CurTime() + offset + util.SharedRandom("NS_IDLE_TIMER", 5, 7.5)
end