--Reimplementation of AvHBasePlayerWeapon.cpp in Garry's Mod SWEP Lua.

if CLIENT then
SWEP.WepSelectIcon = surface.GetTextureID( "ns/hud/lmg" )
SWEP.DrawWeaponInfoBox = false
SWEP.BounceWeaponIcon = false
killicon.Add( "ns_weapon_lmg", "ns/hud/Lmg_killicon", Color( 255, 255, 255, 255 ) )
end

SWEP.PrintName = "Light Machine Gun"
SWEP.Category = "Natural-Selection"
SWEP.Spawnable= true
SWEP.AdminSpawnable= true
SWEP.AdminOnly = false

SWEP.ViewModelFOV = 80
SWEP.ViewModel = "models/ns/weapons/v_mg.mdl"
SWEP.WorldModel = "models/ns/weapons/w_mg.mdl"
SWEP.ViewModelFlip = false
SWEP.BobScale = 0
SWEP.SwayScale = 0

SWEP.AutoSwitchTo = false
SWEP.AutoSwitchFrom = false
SWEP.Weight = 7
SWEP.Slot = 2
SWEP.SlotPos = 0

SWEP.UseHands = false
SWEP.HoldType = "ar2"
SWEP.FiresUnderwater = false
SWEP.DrawCrosshair = true
SWEP.DrawAmmo = true
SWEP.CSMuzzleFlashes = 0
SWEP.Base = "weapon_base"
SWEP.NeedsDryfireNoise = true

SWEP.Primary.Sound = Sound( "Weapon_NS_Lmg.Single" )
SWEP.Primary.ClipSize = 50
SWEP.Primary.DefaultClip = 200
SWEP.Primary.MaxAmmo = 250
SWEP.Primary.Automatic = true
SWEP.Primary.Ammo = "SMG1"
SWEP.Primary.Damage = 10
SWEP.Primary.TakeAmmo = 1
SWEP.Primary.NumberofShots = 1
SWEP.Primary.Spread = 0.03490
SWEP.Primary.Delay = 0.05
SWEP.Primary.Force = 1
SWEP.Secondary.ClipSize = 0
SWEP.Secondary.DefaultClip = 0
SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"

function SWEP:Initialize()
	self:SetWeaponHoldType( self.HoldType )
	self:SetNextIdle()
end

function DrawCrosshairForeground(x, y)
	surface.SetDrawColor( 255, 255, 255, 255 )
	surface.DrawRect( x-1, y-9, 2, 6 ) --Up
	surface.DrawRect( x-1, y+3, 2, 6 ) --Down
	surface.DrawRect( x-9, y-1, 6, 2 ) --Left
	surface.DrawRect( x+3, y-1, 6, 2 ) --Right
end

function DrawCrosshairBackground(x, y)
	surface.SetDrawColor( 0, 0, 0, 255 )
	surface.DrawRect( x-2, y-10, 4, 7 ) --Up
	surface.DrawRect( x-2, y+3, 4, 7 ) --Down
	surface.DrawRect( x-10, y-2, 7, 4 ) --Left
	surface.DrawRect( x+3, y-2, 7, 4 ) --Right
end

function SWEP:DoDrawCrosshair( x, y ) --Draw the NS crosshair without using sprites.
	DrawCrosshairBackground(x, y)
	DrawCrosshairForeground(x, y)
	return true
end

function SWEP:Deploy()
	self:SetWeaponHoldType( self.HoldType )
	self.Weapon:SendWeaponAnim( ACT_VM_DRAW )
	self:SetNextPrimaryFire( CurTime() + .5 )
	self.Reloading = 0
	self.ReloadingTimer = CurTime()
	self:SetNextIdle()
	if self.Owner.NSSkinColor == nil then
		self.Owner.NSSkinColor = math.random(0,1)
	end
	self.Owner:GetViewModel():SetSkin(self.Owner.NSSkinColor)
	return true
end

function SWEP:Holster()
	return true
end

function SWEP:PrimaryAttack()
	if self.Reloading == 1 then return end
	if self.Weapon:Clip1() <= 0 and self.Weapon:Ammo1() <= 0 then
		if self.NeedsDryfireNoise == true then
			self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_EMPTY )
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	elseif self.FiresUnderwater == false and self.Owner:WaterLevel() == 3 then
		if self.NeedsDryfireNoise == true then
			self:EmitSound( "Weapon_NS/weapons.DryFire" )
			self.NeedsDryfireNoise = false
		end
		return
	end
	local bullet = {}
	bullet.Num = self.Primary.NumberofShots
	bullet.Src = self.Owner:GetShootPos()
	bullet.Dir = self.Owner:GetAimVector()
	bullet.Spread = Vector( 1 * self.Primary.Spread, 1 * self.Primary.Spread, 0 )
	bullet.Tracer = 0
	bullet.Force = self.Primary.Force
	bullet.Damage = self.Primary.Damage
	bullet.AmmoType = self.Primary.Ammo
	self.Owner:FireBullets( bullet )
	self:EmitSound( self.Primary.Sound )
	self.Owner:MuzzleFlash()
	if self.Weapon:Clip1() > 0 then
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK )
	else
		self.Weapon:SendWeaponAnim( ACT_VM_PRIMARYATTACK_EMPTY )
	end
	self:TakePrimaryAmmo( self.Primary.TakeAmmo )
	self.Owner:SetAnimation( PLAYER_ATTACK1 )
	self:SetNextPrimaryFire( CurTime() + self.Primary.Delay )
	self:SetNextIdle()
	if self.Weapon:Clip1() <= 0 then
		self:EmitSound( "Weapon_NS/weapons.DryFire" )
		self:Reload()
		return
	end
end

function SWEP:Reload()
	if self.Reloading == 0 and self.Weapon:Clip1() < self.Primary.ClipSize and self.Weapon:Ammo1() > 0 then
		self.Owner:SetAnimation( PLAYER_RELOAD )
		self.Weapon:SendWeaponAnim( ACT_VM_RELOAD )
		self.Reloading = 1
		self.ReloadingTimer = CurTime() + 3.0
		self:SetNextPrimaryFire( self.ReloadingTimer )
		self:SetNextIdle()
		self.IdleTimer = self.IdleTimer + 3.0
	end
end

function SWEP:SecondaryAttack()
end

function SWEP:Think()
	if self.Weapon:Clip1() <= 0 and self.Reloading == 0 and (self.Weapon:GetNextPrimaryFire() < CurTime()) then
		self:Reload()
	end
	if self.Reloading == 1 and self.ReloadingTimer <= CurTime() then
		if self.Weapon:Ammo1() > ( self.Primary.ClipSize - self.Weapon:Clip1() ) then
			self.Owner:SetAmmo( self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1(), self.Primary.Ammo )
			self.Weapon:SetClip1( self.Primary.ClipSize )
		end
		if ( self.Weapon:Ammo1() - self.Primary.ClipSize + self.Weapon:Clip1() ) + self.Weapon:Clip1() < self.Primary.ClipSize then
			self.Weapon:SetClip1( self.Weapon:Clip1() + self.Weapon:Ammo1() )
			self.Owner:SetAmmo( 0, self.Primary.Ammo )
		end
		self.Reloading = 0
	end
	if self.IdleTimer < CurTime() then
		if SERVER then
			self.Weapon:SendWeaponAnim( ACT_VM_IDLE )
		end
		self:SetNextIdle()
	end
	if self.Weapon:Ammo1() > self.Primary.MaxAmmo then
		self.Owner:SetAmmo( self.Primary.MaxAmmo, self.Primary.Ammo )
	end
	if not self.NeedsDryfireNoise and self.Owner:KeyDown(IN_ATTACK) == false then
		self.NeedsDryfireNoise = true
	end
end

function SWEP:SetNextIdle()
	self.IdleTimer = CurTime() + util.SharedRandom("NS_IDLE_TIMER", 5, 7.5)
end